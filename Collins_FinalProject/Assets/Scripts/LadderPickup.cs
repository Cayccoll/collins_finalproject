﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderPickup : MonoBehaviour {

    public GameObject LadderImage;
    public bool ladderpickup = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		/*if(Input.GetKeyDown("f") && ladderpickup == true)
        {
            LadderImage.SetActive(false);
            ladderpickup = false;
            LadderManager.LadderCount += 10;
        }*/
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Player") && LadderImage.activeSelf == false)
        {
            Destroy(gameObject);
            ladderpickup = true;
            LadderImage.SetActive(true);
        }
    }
}

