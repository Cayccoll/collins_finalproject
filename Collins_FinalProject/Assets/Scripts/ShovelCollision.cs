﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShovelCollision : MonoBehaviour {

    public bool destroyed = false;

    public bool isDamaged = false;

    public AudioClip dig;

	// Use this for initialization
	void Start () {
        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().clip = dig;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        destroyed = false;
    }



    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("ground") && !destroyed)
        {
            destroyed = true;
            Destroy(other.gameObject);
            GetComponent<AudioSource>().Play();
        }

        if (other.gameObject.CompareTag("stone") && isDamaged == false)
        {
            Stonescript.Damage = 1;
            
            isDamaged = true;
        }

        if (other.gameObject.CompareTag("stone") && isDamaged == true)
        {
            Destroy(other.gameObject);
        }
            

    }
}
