﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grassSpawner : MonoBehaviour
{


    public int Rows = 1;
    public int Columns = 20;
    public GameObject grass;

    public GameObject[,] grassArray;


    // Use this for initialization
    void Start()
    {
        Spawn();
    }

    void Spawn()
    {
        grassArray = new GameObject[Columns, Rows];

        for (int i = 0; i < Columns; i++)
        {
            for (int j = 0; j < Rows; j++)
            {
                
                
                    grassArray[i, j] = (GameObject)Instantiate(grass, new Vector3(i, j, 0), Quaternion.identity);
                
            }

        }
    }
}