﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyWalk : MonoBehaviour
{
    [HideInInspector] public bool facingRight = false;

    public float velocity = 1f;

    public Transform sightStart;
    public Transform sightEnd;

    public LayerMask detectWhat;

    public bool breakGround = true;

    public bool colliding = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, GetComponent<Rigidbody2D>().velocity.y);

        colliding = Physics2D.Linecast(sightStart.position, sightEnd.position, detectWhat);

        if (colliding)
        {
            velocity *= -1;
            Flip();

        }

    }

    /*void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;

        Gizmos.DrawLine(sightStart.position, sightEnd.position);
    }*/

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            SceneManager.LoadScene("Level_01");
        }

        if (collision.gameObject.CompareTag("ground") && breakGround == true || collision.gameObject.CompareTag("stone") && breakGround == true)
        {
            Destroy(collision.gameObject);
            breakGround = false;
        }
    }

}


