﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LadderManager : MonoBehaviour {

    public static int LadderCount;
    public static int maxLadders;

    Text text;

	// Use this for initialization
	void Awake () {
        maxLadders = 20;
        LadderCount = 10;
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        text.text = "Ladders: "+ LadderCount;
	}
}
