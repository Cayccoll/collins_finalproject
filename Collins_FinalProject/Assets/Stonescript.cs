﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stonescript : MonoBehaviour {

    public static int Damage = 0;

    public Sprite stone2;

	// Use this for initialization
	void Start () {
		
	}
	

	// Update is called once per frame
	void Update () {
		if (Damage == 1)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = stone2;
            
            
        }
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("ground"))
        {
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.CompareTag("Pickup") || collision.gameObject.CompareTag("Gold") || collision.gameObject.CompareTag("Gem"))
            Destroy(collision.gameObject);
    }
}
