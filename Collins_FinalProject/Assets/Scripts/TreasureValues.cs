﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureValues : MonoBehaviour {

    public int Value = 0;


	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            treasureManager.TreasureCount += Value;

            Destroy(gameObject);
            
        }
    }
}
