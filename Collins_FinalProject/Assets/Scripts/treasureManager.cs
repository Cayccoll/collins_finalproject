﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class treasureManager : MonoBehaviour {

    public static int TreasureCount;

    Text text;

	// Use this for initialization
	void Awake () {
		TreasureCount = 0;
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        text.text = "Treasure: " + TreasureCount;
    }
}
