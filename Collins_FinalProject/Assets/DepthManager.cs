﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DepthManager : MonoBehaviour {

    public static int DepthCounter;
    public GameObject player;

    Text text;

    // Use this for initialization
    void Awake()
    {
        //DepthCounter = player.transform.position.y;
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update () {
        DepthCounter = 100 - (int)(player.transform.position.y);
        text.text = "Depth: " + DepthCounter;
	}
}
