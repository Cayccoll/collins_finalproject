﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SimplePlatformController : MonoBehaviour {

    [HideInInspector] public bool facingRight = true;

    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public GameObject Shovel;
    public LadderPickup LadderPickup;
    public GameObject Ladder;

    private Animator anim;
    private Rigidbody2D rb2d;

    public bool signTriggered = false;

    public Collider2D sign;

    public GameObject signtext;

    public bool chestTriggered = false;

    public static int storedTreasure = 0;

    public GameObject failstate;

    public AudioClip sword;
    public AudioClip coin;
    public AudioClip gem;

    // Use this for initialization
    void Awake ()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().clip = sword;
        /*GetComponent<AudioSource>().clip = coin;
        GetComponent<AudioSource>().clip = gem;*/
    }

    // Update is called once per frame
    void Update()
    {
        bool digging = Input.GetKeyDown("s");

        anim.SetBool("Dig", digging);

        bool swinging = Input.GetKey(KeyCode.Space);

        anim.SetBool("Sword", swinging);

        if (Input.GetKeyDown("r"))
        {
            SceneManager.LoadScene("Level_01");
        }

        if (Input.GetKeyDown("q") && LadderPickup.LadderImage.activeSelf == true && LadderManager.LadderCount < LadderManager.maxLadders - 9)
        {
            LadderPickup.LadderImage.SetActive(false);
            LadderManager.LadderCount += 10;
            LadderPickup.ladderpickup = false;
        }

        if (Input.GetKeyDown("w") && LadderManager.LadderCount > 0)
        {
            Vector3 currentPos = new Vector3(Mathf.Round(transform.position.x), Mathf.Round(transform.position.y), Mathf.Round(transform.position.z));
            Instantiate(Ladder, currentPos, transform.rotation);
            LadderManager.LadderCount -= 1;

            var playerPos = transform.position;
            transform.position = new Vector3(Mathf.Round(playerPos.x),
                             Mathf.Round(playerPos.y+1),
                             Mathf.Round(playerPos.z));
        }

        if (Input.GetKeyDown("e") && signTriggered == true)
        {
            signtext.SetActive(true);
        }

        if (signTriggered == false)
        {
            signtext.SetActive(false);
        }

        if (Input.GetKeyDown("f") && chestTriggered == true && treasureManager.TreasureCount >= 2)
        {
            treasureManager.TreasureCount -= 2;
            LadderManager.maxLadders += 5;
            LadderManager.LadderCount += 5;
            storedTreasure += 2;
        }

        if (DepthManager.DepthCounter >= 1 && LadderManager.LadderCount == 0 && LadderPickup.ladderpickup == false)
        {
            failstate.SetActive(true);
        }
        else
        {
            failstate.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<AudioSource>().Play();
        }

    }

    private void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        anim.SetFloat("Speed", Mathf.Abs(h));

        if (h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveForce);

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();

        /*bool digging = Input.GetKey("s");

        anim.SetBool("Dig", digging);

        bool swinging = Input.GetKey(KeyCode.Space);

        anim.SetBool("Sword", swinging);*/
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Sign"))
        {
            signTriggered = true;
        }

        if (other.gameObject.CompareTag("chest"))
        {
            chestTriggered = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Sign"))
        {
            signTriggered = false;
        }

        if (other.gameObject.CompareTag("chest"))
        {
            chestTriggered = false;
        }
    }

    /*private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Gold"))
        {
            GetComponent<AudioSource>().Play();
        }

        if (collision.gameObject.CompareTag("Gem"))
        {
            GetComponent<AudioSource>().Play();
        }
    }*/


    /*   private void OnTriggerEnter2D(Collider2D ground)
       {
           if (ground.gameObject.CompareTag("other"))
               Destroy(other.gameObject);
       }*/

}
