﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    public int Rows = 100;
    public int Columns = 50;
    public GameObject ground;

    public GameObject[,] groundArray;

    public GameObject pickup;

    public GameObject gold;

    public GameObject gem;

    public GameObject enemy;

    public GameObject background;

    public GameObject Stone;


    private Vector2 originPosition;

	// Use this for initialization
	void Start () {

        originPosition = transform.position;
        Spawn();

      
		
	}
	
	// Update is called once per frame
	void Spawn () {
        groundArray = new GameObject[Columns, Rows];

        for (int i = 0; i < Columns; i++)
        {
            for(int j = 0; j < Rows; j++)
            {

                groundArray[i, j] = (GameObject)Instantiate(background, new Vector3(i, j, 0), Quaternion.identity);

                if (Random.value < .7 || j == 99 && j != 100)
                {
                    groundArray[i,j] =(GameObject)Instantiate(ground, new Vector3(i,j,0),Quaternion.identity);
                }

                if (Random.value > .7)
                {
                    groundArray[i, j] = (GameObject)Instantiate(pickup, new Vector3(i, j, 0), Quaternion.identity);

                }

                if (Random.value > .7)
                {
                    groundArray[i, j] = (GameObject)Instantiate(gold, new Vector3(i, j, 0), Quaternion.identity);

                }

                if (Random.value > .7)
                {
                    groundArray[i, j] = (GameObject)Instantiate(gem, new Vector3(i, j, 0), Quaternion.identity);

                }

                if (Random.value > .98)
                {
                    groundArray[i, j] = (GameObject)Instantiate(enemy, new Vector3(i, j, 0), Quaternion.identity);

                }

                /*if (Random.value > .2 && j <= 50)
                {
                    groundArray[i, j] = (GameObject)Instantiate(Stone, new Vector3(i, j, 0), Quaternion.identity);

                }*/




            }
        }
		
	}

}
